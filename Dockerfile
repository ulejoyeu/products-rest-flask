FROM python:3.11.3-slim-buster
WORKDIR /app
COPY web /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
EXPOSE 3000
CMD ["python", "app.py"]
